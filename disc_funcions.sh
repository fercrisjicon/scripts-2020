#! /bin/bash

function exemple_filtra_GID(){
  MAX=500
  # validar numero d'arguments 
  if [ $# -ne 1 ]
  then
    echo "error:..."
    return 1
  fi

  file=$1
  # llegir contingut del fitxer
  while read -r linea
  do
    gid=$(echo $linea | cut -d: -f4)
    # valida el gid si es gran o igual 500
    if [ $gid -ge $MAX ]
    then
      # xixa
      echo $linea
    fi
  done < $file
}

function exemple_llistar(){
  num=1
  if [ $# -ne 1 ]
  then
    echo "error:..."
  fi
  file=$1
  while read -r login
  do
    echo "$num: $login"
    ((num++))
  done < $file
}

function exemple_llistar2(){
  num=1
  file=/dev/stdin
  if [ $# -eq 1 ]
  then
    file=$1
  fi
  while read -r login
  do
    echo "$num: $login"
    ((num++))
  done < $file
}
