#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Copia file al dirdesti
#       prog.sh file[...] dirdesti
# Validar num arguments
# Mostrar les opcions
# Mostrar els arguments
# -------------------------------------------
ERR_NARGS=1

# 1) validar num d'arguments
if [ $# -lt 1 ]; then
  echo "Error: num arg incorrecte"
  echo "Usage: $0 opcions[...] arguments[...]"
  exit $ERR_NARGS
fi

opcions=''
arguments=''
# xixa
for elem in $*
do
  case $elem in
    "-a"|"-b"|"-c"|"-d"|"-e"|"-f"|"-g")
      opcions="$opcions $elem";;
    *)
      arguments="$arguments $elem"
  esac
done

echo "Opcions: $opcions"
echo "Arguments: $arguments"
exit 0
