#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Copia file al dirdesti
#       prog.sh file[...] dirdesti
# Validar num arguments
# Validar directori destí
# Validar cada fitxer
# Copiar cada fitxer al directori destí
# -------------------------------------------
status=0
ERR_NARGS=1
ERR_NDIR=2

# 1) validar num d'arguments
if [ $# -lt 2 ]; then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 file[...] dirdestí"
  exit $ERR_NARGS
fi

mydir=$( echo $* | cut -d' ' -f$# )
# 2) validar el directori desti
if [ ! -d $mydir ]; then
  echo "Error: directori $mydir no existeix"
  echo "Usage: $0 file[...] dirdestí"
  exit $ERR_NDIR
fi

lst_file=$( echo $* | cut -d' ' -f1-$(($#-1)) )
# per cada fitxer
for elem in $lst_file
do
  # 3) validar si es un file
  if [ ! -e $elem ]; then
    echo "Error: file $elem no existeix" >> /dev/stderr
  else
  # xixa 
    cp $elem $mydir  
  fi
done
exit 0
