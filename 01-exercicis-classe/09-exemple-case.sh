#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Exemples case
# -------------------------------------------
# dl dt dc dj dv ds dm
case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
    echo "es una dia laborable";;
  "ds"|"dm")
     echo "es un dia festiu";;
   *)
     echo "es altra cosa"
esac

exit 0

# dir si es una vocal, consonant o altra cosa
case $1 in
  [aeiou])
    echo "es una vocal";;
  [bcdfghjklmnpqrstvwxyz])
    echo "es una consonant";;
  *)
    echo "es una altre cosa"
esac

exit 0

# 1) exemple noms
case $1 in
  "pere"|"pau"|"jona")
    echo "es un nen";;
  "marta"|"anna"|"julia")
    echo "es una nena"
    ;;
  *)
    echo "indefinit"
esac
exit 0
