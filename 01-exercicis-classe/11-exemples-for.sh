#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Exemples de bucle for
# -------------------------------------------
# for var in llista_valors
# do
# 	accions
# done

# $* $@ es diferencien que "$@" s'expandeix igualment

# 6) llistar numerats tots els login
logins=$(cut -d: -f1 /etc/passwd)
num=1
for login in $logins
do
  echo "$num: $login"
  ((num++))
done
exit 0

# 5) llistat de noms del directori actiu numerat linia a linia
llista=$(ls)
num=1
for file in $llista
do
  echo "$num- $file"
  ((num++))
done
exit 0

# 4) llistar els arguments numerats
num=1
for arg in $*
do
  echo "$num- $arg"
  num=$((num + 1))
done
exit 0

# 3) iterar per la llista de noms de fitxers que genera ls
llista=$(ls)
for file in $llista
do
  echo $file
done
exit 0

# 2b) iterar per cada argument rebut
for arg in "$@"
do
  echo $arg
done
exit 0

# 2a) iterar per cada argument rebut
for arg in "$*"
do
  echo $arg
done
exit 0

# 1b) iterar una llista de noms
for nom in "pere marta anna ramon"
do
  echo $nom
done
exit 0

# 1a) iterar una llista de noms
for nom in "pere" "marta" "anna" "ramon" # tambe funciona sense cometes dobles
do      
  echo $nom
done
exit 0
