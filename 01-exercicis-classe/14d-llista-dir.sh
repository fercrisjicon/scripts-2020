#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Llistar un directori
# 	prog.sh dir[...]
# Validar arguments
# Validar si es un directori
# Llistat del directori
# -------------------------------------------
status=0
ERR_NARGS=1
ERR_NDIR=2

# 1) validar num arguments
if [ $# -lt 1 ]; then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 dir[..]"
  exit $ERR_NARGS
fi

for mydir in $*
do
  # 2) validar si es un directori
  if ! [ -d $mydir ]; then
    echo "Error: dir $mydir no existeix" >> /dev/stdin
  else
    # xixa
    echo "Directori: $mydir"
    llista=$(ls $mydir)
    for elem in $llista
    do
      if [ -h $mydir/$elem ]; then
        echo -e "\t$elem es un link"
      elif [ -d $mydir/$elem ]; then
	echo -e "\t$elem es un directori"
      elif [ -f $mydir/$elem ]; then
	echo -e "\t$elem es un regular file"
      else
        echo -e "\$elem es un altre cosa"
      fi
    done
  fi
done
exit $status
