#! /bin/bash
# isx41016667 M01-ISO
# Març 2021
# Mostrar opcions amb valor i arguments
#       prog.sh [ -a file -b -c -bnum -e ] args[..]
# Validar num arguments
# Mostrar les opcions amb valors
# Mostrar els arguments
# -------------------------------------------
ERR_NARGS=1

opcions=""
args=""
file=""
min=""
max=""
while [ -n "$1" ]
do
  case $1 in
    -[bce])
      opcions="$opcions $1";;
    -a)
      file=$2
      shift;;
    -d)
      min=$2
      max=$3
      shift
      shift;;
    *)
      args="$args $1"
  esac
  shift 
done

echo "Opcions: $opcions"
echo "Arguments: $args"
echo "File: $file"
echo "MAX MIN: $max $min"

exit 0

