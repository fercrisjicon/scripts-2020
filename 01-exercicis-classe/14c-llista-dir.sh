#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Llistar un directori
# 	prog.sh dir
# Validar arguments
# Validar si es un directori
# Llistat del directori
# -------------------------------------------
OK=0
ERR_NARGS=1
ERR_NDIR=2

# 1) validar num arguments
if [ $# -ne 1 ]; then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 dir"
  exit $ERR_NARGS
fi

# 2) validar si es un directori
if ! [ -d $1 ]; then
  echo "Error: dir $1 no existeix"
  echo "Usage: $0 dir"
  exit $ERR_NDIR
fi

mydir=$1
# xixa
llista=$(ls $mydir)
for elem in $llista
do
  if [ -h $mydir/$elem ]; then
    echo "$elem es un link"
  elif [ -d $mydir/$elem ]; then
    echo "$elem es un directori"
  elif [ -f $mydir/$elem ]; then
    echo "$elem es un regular file"
  else
    echo "$elem es un altre cosa"
  fi
done
exit $OK
