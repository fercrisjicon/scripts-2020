#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Mostrar els dies d'un mes rebut
# per arguments.
#       prog.sh num[1-12]
# Validar num argument
# Validar mes del any
# Mostrar dies del mes
# -------------------------------------------
ERR_NARGS=1
ERR_NMES=2
# 1 validar num arguments
if [ $# -ne 1 ]
then
  echo "Error: num argument incorrecte"
  echo "Usage: $0 num[1-12]"
  exit $ERR_NARGS
fi

mes=$1
# 2 validar mes del any
if ! [ $mes -ge 1 -a $mes -le 12 ]
then
  echo "Error: mes $mes no existeix"
  echo "Usage: $0 num[1-12]"
  exit $ERR_NMES
fi

# xixa
case $mes in
  2)
    echo "28";;
  1|3|5|7|8|10|12)
    echo "31";;
  *)
    echo "30"
esac
exit 0
