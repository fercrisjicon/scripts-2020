#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Rep per arguments logins, si existeixen
# mostra login.
#       prog.sh logins[...]
# Validar num argument
# Validar login
# Mostrar login
# -------------------------------------------
ERR_NARGS=1
ERR_NLOGIN=2
# 1 validar num arguments
if [ $# -lt 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 logins[...]"
  exit $ERR_NARGS
fi

LOGINS=$*
for user in $LOGINS
do
  # 2 validar login
  grep "^$login:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]
  then
    # xixa
    echo $login
  else
    echo $login >> /dev/stderr
  fi
done
exit 0
