#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Rep per stdin logins, si existeixen
# mostra login.
#       prog.sh logins[...]
# Validar num argument
# Validar login
# Mostrar login
# -------------------------------------------
# 1 validar linia per stdin
while read -r login
do
  # 2 validar login
  grep "^$login:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]
  then
    # xixa
    echo $login
  else
    echo $login >> /dev/stderr
  fi
done
exit 0
