#! /bin/bash
# isx41016667 M01-ISO
# Febrer 2021
# Mostra linia a linia rebut per argument
#       prog.sh args[...]
# Validar arguments
# Numera linia a linia
# -------------------------------------------
ERR_NARGS=1
# 1 validar num arguments
if [ $# -lt 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args[...]"
  exit $ERR_NARGS
fi
LINIAS=$*

num=0
for linia in $LINIAS
do
  # xixa
  echo "$num- $linia"
  ((num++))
done
exit 0
