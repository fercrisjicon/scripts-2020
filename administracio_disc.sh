#! /bin/bash
# Cristian Condolo
# Administracio de disc
# isx41016667 ISO 2020-21
# ==================================
# 1. fsize() login
function fsize(){
  ERR_NARGS=1
  ERR_NLOGIN=2
  # 1 validar argument
  if [ $# -ne 1 ] 
  then
    echo "Error: num arguments incorrecte"
    echo "Usage: fsize login"
    return $ERR_NARGS
  fi
  login=$1

  # 2 verificar login
  home=$(grep "^$login:" /etc/passwd | cut -d: -f6)
  if [ -z $home ] 
  then
    echo "Error: login $login inexistent"
    echo "Usage: fsize login"
    return $NLOGIN
  fi

  # xixa
  du -h $home 2> /dev/null
  return 0
}

# 2. loginargs login[...]
function loginargs(){
  ERR_NARGS=1
  # 1 validar arguments
  if [ $# -lt 1 ]
  then
    echo "Error: num arguments incorrecte"
    echo "Usage: loginargs login[...]"
    return $ERR_NARGS
  fi
  LOGINS=$*

  for login in $LOGINS
  do
    # 2 validar login
    grep "^$login:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]
    then
      echo "Error: login $login inexistent" >> /dev/stderr
    else
      # xixa
      fsize $login
    fi
  done
  return 0
}

# 3. loginfile file.txt
function loginfile(){
  ERR_NARGS=1
  ERR_NRFILE=2
  # 1 valida argument
  if [ $# -ne 1 ]
  then
    echo "Error: num arguments incorrecte"
    echo "Usage: loginargs file.txt"
    return $ERR_NARGS
  fi
  file=$1

  # 2 validar regular file
  if ! [ -f $file ]
  then
    echo "Error: $file no es regular file"
    echo "Usage: loginargs file.txt"
    return $ERR_NRFILE
  fi

  while read -r login
  do
    # xixa
    fsize $login
  done < $file
  return 0
}

# 4. loginboth file.txt(or none)
function loginboth(){
  file=/dev/stdin
  # 1 validar argument
  if [ $# -eq 1 ]
  then
    file=$1
  fi
  
  while read -r login
  do
    # 2 validar login
    grep "^$login:" /etc/passwd &> /dev/null
    if [ $? -ne 0 ]
    then
      echo "Error: $login no existeix" >> /dev/stderr
    else
      fsize $login
    fi
  done < $file
  return 0
}

# 9. fstype() arg1
function fstype(){
  ERR_NARGS=1
  ERR_NFSTYOE=2
  # 1 validar argument
  if [ $# -ne 1 ]
  then
    echo "Error: num arguments incorrecte"
    echo "Usage: fstype arg1"
    return $ERR_NARGS
  fi
  fstype=$1

  # xixa
  devices=$(egrep -v '^#|^$' /etc/fstab | tr -s '[:blank:]' ' ')
  echo "$devices" | grep "^[^ ]* [^ ]* $fstype " | cut -d' ' -f1,2 | sort -t' ' -k1,1
  return 0
}

# 10. allfstype() num
function allfstype(){
  ERR_NARGS=1
  # 1 validar num arguments
  if [ $# -ne 1 ]
  then
    echo "Error: num arguments incorrecte"
    echo "Usage: allfstype num"
    return $ERR_NARGS
  fi
  MIN=$1

  devices=$(egrep -v '^#|^$' /etc/fstab | tr -s '[:blank:]' ' ')
  lst_fstype=$(echo "$devices" | cut -d' ' -f3)
  for fstype in $lst_fstype
  do
    num=$(echo "$devices" | grep "^[^ ]* [^ ]* $fstype " | wc -l)
    # 2 validar minim
    echo "$fstype($num):"
    echo "$devices" | grep "^[^ ]* [^ ]* $fstype " | cut -d' ' -f1,2 | sort -t' ' -k1,1 | sed 's/^/\t/g'
  done
  return 0
}
