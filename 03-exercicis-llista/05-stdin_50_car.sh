#! /bin/bash
# isx41016667 M01-ISO
# Març 2021
# Processar stdin mostrant per stdout les
# línies de menys de 50 caràcters.
#       prog.sh
# Validar linies per stdin
# Mostra linies menors de 50 caracters
# -------------------------------------------
# 1 validar linies per stdin
while read -r linia
do
  # xixa
  echo $linia | egrep '^.{,50}$'
done
exit 0
