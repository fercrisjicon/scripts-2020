#! /bin/bash
# isx41016667 M01-ISO
# Març 2021
# Processar els arguments i mostrar per stdout 
# només els de 4 o més caràcters.
#       prog.sh args[...]
# Validar num arguments
# Mostra arguments de mes de 4 caracters
# -------------------------------------------
ERR_ARGS=1
# 1 validar num arguments
if [ $# -lt 1 ]
then
  echo "Error: num arguments incorrecte"
  echo "Usage: $0 args[...]"
  exit $ERR_NARGS
fi

for arg in $*
do
  # xixa
  echo $arg | egrep '.{4,}'
done
exit 0
