#! /bin/bash
# isx41016667 M01-ISO
# Març 2021
# Validar el tipus de fitxer als fitxers
# rebuts per arguments, segon el tipus
# que volem validar.
#       prog.sh -f|-d arg1 arg2 arg3 arg4
#	prog.sh -h|--help
# Validar num arguments
# Validar --help
# Validar tipus d'arguments
# -------------------------------------------
ERR_NARGS=1
# 1 validar num arguments
if [ "$#" -ne 5 ]
then
  echo "ERROR numero args"
  echo "usage: 07_valida_arg.sh -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_NARG
fi

# 2 validar --help
if [ "$1" != "-f" -a "$1" != "-d"  ]
then
  echo "ERROR format arguments"
  echo "usage: 07_valida_arg.sh -f|-d arg1 arg2 arg3 arg4"
  exit $ERR_NARG
fi
tipus=$1
shift

for arg in $*
do
  # xixa
  if ! [ $tipus "$arg" ]; then
    status=2
  fi
done
exit $status

