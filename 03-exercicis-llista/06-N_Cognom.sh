#! /bin/bash
# isx41016667 M01-ISO
# Març 2021
# Processar per stdin linies d’entrada tipus
# “Tom Snyder” i mostrar per stdout la línia
# en format → T. Snyder.
#       prog.sh
# Validar linies per stdin
# Mostra nom y cognom en format: N. Cognom
# -------------------------------------------
# 1 validar linies per stdin
while read -r linia
do
  # xixa
  echo $linia | sed -r 's/^(.)[^ ]* /\1. /'
done
exit 0
