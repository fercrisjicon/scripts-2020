#! /bin/bash
# isx41016667 M01-ISO
# Març 2021
# Processar stdin cmostrant per stdout les 
# línies numerades i en majúscules.
#       prog.sh
# Validar linies per stdin
# Enumerar les linies en majuscules
# -------------------------------------------
num=0
# 1 validar linies per stdin
while read -r linia
do
  # xixa
  ((num++))
  echo $num $linia | tr '[:lower:]' '[:upper:]' 
done
exit 0
